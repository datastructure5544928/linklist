/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package linkedlist;

/**
 *
 * @author informatics
 */
public class Link {
    
// linkList.java
// demonstrates linked list
// to run this program: C>java LinkListApp
////////////////////////////////////////////////////////////////


public int iData; // data item (key)
public double dData; // data item
public Link next; // next link in list
// -------------------------------------------------------------

public Link(int id, double dd) // constructor
{
iData = id; // initialize data
dData = dd; // (‘next’ is automatically
} // set to null)
// -------------------------------------------------------------
public void displayLink() // display ourself
{
System.out.print("{" + iData + ", " + dData + "} ");}
} // end class Link
////////////////////////////////////////////////////////////////





