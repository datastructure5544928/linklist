/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package linkedlist;

/**
 *
 * @author informatics
 */
public class LinkList {

    private Link first; // ref to first link on list
    private Link now ;
    
// -------------------------------------------------------------


    public LinkList() // constructor
    {
        first = null; // no items on list yet
    }
// -------------------------------------------------------------

    public boolean isEmpty() // true if list is empty
    {
        return (first == null);
    }
// -------------------------------------------------------------
// insert at start of list

    public void insertFirst(int id, double dd) { // make new link
        Link newLink = new Link(id, dd);
        newLink.next = first;// newLink --> old first
        first = newLink; // first --> newLink
    }
// -------------------------------------------------------------

    public Link deleteFirst() // delete first item
    { // (assumes list not empty)      
        Link temp = first; // save reference to link
        first = first.next; // delete it: first-->old next     
        return temp; // return deleted link
    }
    
    public Link deleteLast() {
        //now = first;
        Link current = first; // start at beginning of list  
              
        //first = first.next;
        while (current.next != first) {
            //if (current.next != null) {
            first = current.next; // move to next link 
            if(first!=null){
                return current;                
            }           
        }
        current.next = null;
        first = null;
        return current;

    }

// -------------------------------------------------------------
    public void displayList() {
        System.out.print("List (first-->last): ");
        Link current = first; // start at beginning of list
        while (current != null) // until end of list,
        {
            current.displayLink(); // print data
            current = current.next; // move to next link
        }
        System.out.println("");
    }
}
